package com.nuteee.bot;

import com.nuteee.bot.db.entities.MatchmakingQueue;
import net.dv8tion.jda.api.entities.Member;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public class MatchmakingQueueTest {

    private static final String ID = "id";
    private static final String DUO_STR = "duo";
    private static final String MULTIPLE_STR = "duo trio squad";
    private static final String WRONG_STR = "asdasd";
    private final MatchmakingQueue duoQueue = new MatchmakingQueue(ID, "Duo", 2, DUO_STR);
    private final MatchmakingQueue trioQueue = new MatchmakingQueue(ID, "Trio", 3, "trio");

    private final Member member1 = Mockito.mock(Member.class);
    private final Member member2 = Mockito.mock(Member.class);

    @Before
    public void init() {
        duoQueue.add(member1).add(member2);
    }

    @Test
    public void testGetFirstMembers() {
        List<Member> expected = Arrays.asList(member1, member2);

        assertEquals(expected, duoQueue.getFirstMembers());
    }

    @Test
    public void testIsMessageEligibleToJoin() {
        assertTrue(duoQueue.isMessageEligibleToJoin(DUO_STR));
        assertFalse(duoQueue.isMessageEligibleToJoin(WRONG_STR));
        assertTrue(trioQueue.isMessageEligibleToJoin(MULTIPLE_STR));
    }

    @Test
    public void testRemoveMemberFromQueue() {
        assertTrue(duoQueue.removeMemberFromQueue(member1));
        assertFalse(duoQueue.removeMemberFromQueue(member1));
    }

    @Test
    public void testToString() {
        System.out.println(duoQueue.toString());
        duoQueue.removeMemberFromQueue(member2);
        System.out.println(duoQueue.toString());
        System.out.println(trioQueue.toString());
    }

    @Test
    public void testEquals() {
        assertEquals(duoQueue, new MatchmakingQueue(ID, "Duo", 2, DUO_STR));
        assertNotEquals(duoQueue, new MatchmakingQueue(ID, "Duo", 5, DUO_STR));
        assertNotEquals(trioQueue, duoQueue);
    }

}
