package com.nuteee.bot;

import com.nuteee.bot.entities.MatchmakingQueuesHandler;
import com.nuteee.bot.eventlisteners.GuildEventListener;
import com.nuteee.bot.eventlisteners.MessageListener;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.security.auth.login.LoginException;


@Configuration
public class BotConfig {

    @Value("${token}")
    private String token;

    @Bean
    public GuildEventListener guildEventListener() {
        return new GuildEventListener();
    }

    @Bean
    public MessageListener guildMessageListener() {
        return new MessageListener();
    }

    @Bean
    public MatchmakingQueuesHandler matchmakingQueuesHandler() {
        return new MatchmakingQueuesHandler();
    }

    @Bean
    public JDA jdaBotService(@Qualifier("guildEventListener") GuildEventListener guildEventListener,
                             @Qualifier("guildMessageListener") MessageListener guildMessageListener)
            throws LoginException, RateLimitedException {
        return new JDABuilder(AccountType.BOT)
                .setToken(token)
                .addEventListeners(guildEventListener, guildMessageListener)
                .build();
    }
}
