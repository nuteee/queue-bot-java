package com.nuteee.bot.eventlisteners;

import com.google.common.collect.Lists;
import com.nuteee.bot.db.entities.GuildConfig;
import com.nuteee.bot.db.repositories.GuildConfigRepository;
import com.nuteee.bot.db.repositories.TeamsCountRepository;
import com.nuteee.bot.entities.BotUtils;
import com.nuteee.bot.entities.MatchmakingQueuesHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;


public class GuildEventListener implements EventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuildEventListener.class);

    @Autowired
    private GuildConfigRepository guildConfigRepository;

    @Autowired
    private MatchmakingQueuesHandler matchmakingQueuesHandler;

    @Autowired
    private TeamsCountRepository teamsCountRepository;

    @Override
    public void onEvent(GenericEvent event) {
        if (event instanceof ReadyEvent) {
            LOGGER.info("Booting up.");

            event.getJDA().getGuilds().forEach((Guild guild) -> {
                LOGGER.info("{} => {}", guild.getId(), guild.getName());

                if (!guildConfigRepository.exists(guild.getId())) {
                    LOGGER.info("Guild config doesn't exist yet, creating a default config");

                    initDefaultConfigForGuild(guild);
                } else {
                    LOGGER.info("Found the config!");

                }
                matchmakingQueuesHandler.initQueuesForGuild(guild);

                GuildConfig guildConfig = guildConfigRepository.findOne(guild.getId());

                if (guildConfig.getIsGuildReady()) {

                    TextChannel lfgTextChannel = BotUtils.getLfgTextChannel(guild, guildConfig, false);
                    LOGGER.debug("Found the LFG text channel: [{}]", lfgTextChannel);

                    // Delete previous bot messages
                    lfgTextChannel.getIterableHistory().complete().stream().limit(100)
                            .filter((Message message) -> message.getAuthor().getId()
                                    .equals(event.getJDA().getSelfUser().getId()))
                            .collect(Collectors.toList()).forEach((Message message) -> message.delete().queue());

                    BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, guildConfig, lfgTextChannel,
                            matchmakingQueuesHandler);

                    updateGameOfBot(event.getJDA(), teamsCountRepository.findOne(1).getTeamsFound());
                } else {
                    // TODO: Send warning to guild owner to configure the bot or the bot will leave
                    // after X days
                }
            });
            LOGGER.info("Booting successful!");
        } else if (event instanceof GuildJoinEvent) {

            LOGGER.info("Joining [{}] => [{}]", ((GuildJoinEvent) event).getGuild().getId(),
                    ((GuildJoinEvent) event).getGuild().getName());

            initDefaultConfigForGuild(((GuildJoinEvent) event).getGuild());

            LOGGER.info("Joined [{}] => [{}]", ((GuildJoinEvent) event).getGuild().getId(),
                    ((GuildJoinEvent) event).getGuild().getName());

        } else if (event instanceof GuildLeaveEvent) {
            LOGGER.info("Leaving [{}] => [{}]", ((GuildLeaveEvent) event).getGuild().getId(),
                    ((GuildLeaveEvent) event).getGuild().getName());

            GuildConfig guildConfig = guildConfigRepository.findOne(((GuildLeaveEvent) event).getGuild().getId());

            LOGGER.info("Deleting config");

            LOGGER.debug("{}", guildConfig.toString());

            guildConfigRepository.delete(guildConfig);
            matchmakingQueuesHandler.removeMessageIdForGuild(((GuildLeaveEvent) event).getGuild().getId());

            LOGGER.info("Left guild [{}] => [{}]", ((GuildLeaveEvent) event).getGuild().getName(),
                    ((GuildLeaveEvent) event).getGuild().getId());

        } else if (event instanceof GuildVoiceLeaveEvent) {
            handleVoiceChange(((GuildVoiceLeaveEvent) event).getGuild(),
                    ((GuildVoiceLeaveEvent) event).getChannelLeft(), ((GuildVoiceLeaveEvent) event).getMember());

        } else if (event instanceof GuildVoiceMoveEvent) {
            handleVoiceChange(((GuildVoiceMoveEvent) event).getGuild(), ((GuildVoiceMoveEvent) event).getChannelLeft(),
                    ((GuildVoiceMoveEvent) event).getMember());
        }

    }

    private void handleVoiceChange(Guild guild, VoiceChannel channelLeft, Member member) {
        GuildConfig guildConfig = guildConfigRepository.findOne(guild.getId());

        if (channelLeft.equals(BotUtils.getLfgVoiceChannel(guild, guildConfig, false))) {
            matchmakingQueuesHandler.getQueuesForGuild(guild).forEach(queue -> queue.removeMemberFromQueue(member));

            BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, guildConfig,
                    BotUtils.getLfgTextChannel(guild, guildConfig, false), matchmakingQueuesHandler);
        }
    }

    public static void updateGameOfBot(JDA jda, Integer teamsFound) {
        jda.getPresence().setActivity(Activity.playing("Found " + teamsFound.toString() + " teams!"));
    }

    private void initDefaultConfigForGuild(Guild guild) {
        String guildOwnerId = guild.getOwner().getUser().getId();
        GuildConfig gc = GuildConfig.getDefaultGuildConfigForGuildIdAndManagers(guild.getId(),
                Lists.newArrayList(guildOwnerId));

        guildConfigRepository.save(gc);

        gc.getMatchmakingQueues().forEach(queue -> matchmakingQueuesHandler.addQueueForGuild(guild.getId(), queue));

        sendGreetingMessageWithDescriptionToOwnerOfGuild(guild, gc);
    }

    private void sendGreetingMessageWithDescriptionToOwnerOfGuild(Guild guild, GuildConfig gc) {
        StringBuilder sb = new StringBuilder();

        sb.append("Queue bot joined but you need to set the following commands first.\n");
        sb.append(
                "Please set the default LFG text channel (cmd: `!setLfgTextChannelName`) and the default LFG voice channel (cmd: `!setLfgVoiceChannelName`).\n");
        sb.append(
                "You can add queues with the `!addQueue [NAME] [LENGTH] [JOIN COMMAND]` command. E.g.: `!addqueue Trio 3 trio` and people can join this queue with the `!join trio` command\n");
        sb.append("You can remove queues with the `!removeQueue [NAME]` command. E.g.: `!removequeue Trio`\n");
        sb.append(
                "To add managers use the `!addmanager [Users mentioned here]` command. This will add each mentioned user as managers of this bot on that server.\n");
        sb.append("Use `!stats` for help and set the values with !set{propertyname} (without space)\n");
        sb.append("\n==========================\n");
        sb.append("Actual (default) server config for " + guild.getName() + ":\n");
        sb.append(gc.getPropertyValues());

        LOGGER.debug("Sending greeting message to [{}] => [{}]", guild.getOwner().getUser().getName(),
                guild.getOwner().getUser().getId());

        guild.getOwner().getUser().openPrivateChannel().queue(
                // success callback
                (PrivateChannel privateChannel) -> privateChannel.sendMessage(sb.toString()).queue(),
                // error callback
                (Throwable t) -> LOGGER.error(
                        "Couldn't send greeting message to guild owner [{}] for guild [{}] with ID: [{}]",
                        guild.getOwner().getUser().getName(), guild.getName(), guild.getId()));
    }

}
