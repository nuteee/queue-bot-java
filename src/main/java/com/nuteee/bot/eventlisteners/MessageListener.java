package com.nuteee.bot.eventlisteners;

import com.google.common.collect.Lists;
import com.nuteee.bot.db.entities.GuildConfig;
import com.nuteee.bot.db.entities.Manager;
import com.nuteee.bot.db.entities.MatchmakingQueue;
import com.nuteee.bot.db.entities.TeamsCount;
import com.nuteee.bot.db.repositories.GuildConfigRepository;
import com.nuteee.bot.db.repositories.TeamsCountRepository;
import com.nuteee.bot.entities.BotUtils;
import com.nuteee.bot.entities.MatchmakingQueuesHandler;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class MessageListener extends ListenerAdapter {

    private static final String DONE_MESSAGE = "Done!";

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);

    @Autowired
    private GuildConfigRepository guildConfigRepository;

    @Autowired
    private MatchmakingQueuesHandler matchmakingQueuesHandler;

    @Autowired
    private TeamsCountRepository teamsCountRepository;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Guild guild = event.getGuild();
        Member author = event.getMember();
        Message message = event.getMessage();

        if (event.getAuthor().isBot())
            return;

        GuildConfig gc = guildConfigRepository.findOne(guild.getId());

        if (message.getContentStripped().equalsIgnoreCase("ping")) {
            LOGGER.info("Ping requested by [{}] - [{}]", author.getUser().getName(), author.getUser().getId());
            BotUtils.replyToMessage(message, "Pong!");
            return;
        }

        TextChannel lfgTextChannel = BotUtils.getLfgTextChannel(guild, gc, false);
        VoiceChannel lfgVoiceChannel = BotUtils.getLfgVoiceChannel(guild, gc, false);
        if (message.getContentStripped().trim().startsWith(gc.getCmdPrefix())) {
            List<String> tmp = Lists.newArrayList(message.getContentStripped().split("\\s+"));

            if (tmp.get(0).equals(gc.getCmdPrefix() + "stats")) {
                message.getTextChannel().sendMessage(gc.getPropertyValues()).queue();
                return;
            }

            if (tmp.size() >= 2) {

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setLfgVoiceChannel".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    List<VoiceChannel> possibleChannels = guild.getVoiceChannelsByName(tmp.get(1), true);

                    if (possibleChannels.size() != 1) {
                        LOGGER.warn("{} voicechannels found for voicechannel: [{}] - [{}] - [{}]",
                                possibleChannels.isEmpty() ? "Zero" : "Multiple", tmp.get(1), guild.getName(),
                                guild.getId());
                        String responseMessage = String.format(
                                "%s voicechannels found for voicechannel: [%s] - [%s] - [%s]",
                                possibleChannels.isEmpty() ? "Zero" : "Multiple", tmp.get(1), guild.getName(),
                                guild.getId());
                        BotUtils.replyToMessage(message, responseMessage);
                    } else {
                        gc.setLfgVoiceChannel(tmp.get(1));
                        guildConfigRepository.save(gc);
                        LOGGER.info("Successfully updated the LFG voice channel for guild [{}] - [{}]", guild.getName(),
                                guild.getId());
                        BotUtils.replyToMessage(message, DONE_MESSAGE);

                        if (BotUtils.isGuildReady(guild, gc)) {
                            gc.setIsGuildReady(true);
                            guildConfigRepository.save(gc);
                            matchmakingQueuesHandler.initQueuesForGuild(guild);
                            lfgTextChannel = BotUtils.getLfgTextChannel(guild, gc, true);
                            BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, gc, lfgTextChannel,
                                    matchmakingQueuesHandler);
                        }
                    }

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setLfgTextChannel".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    List<TextChannel> possibleChannels = guild.getTextChannelsByName(tmp.get(1), true);

                    if (possibleChannels.size() != 1) {
                        LOGGER.warn("{} textchannels found for textchannel: [{}] - [{}] - [{}]",
                                possibleChannels.isEmpty() ? "Zero" : "Multiple", tmp.get(1), guild.getName(),
                                guild.getId());
                        String responseMessage = String.format(
                                "%s textchannels found for textchannel: [%s] - [%s] - [%s]",
                                possibleChannels.isEmpty() ? "Zero" : "Multiple", tmp.get(1), guild.getName(),
                                guild.getId());
                        BotUtils.replyToMessage(message, responseMessage);
                    } else {
                        gc.setLfgTextChannel(tmp.get(1));
                        guildConfigRepository.save(gc);
                        LOGGER.info("Successfully updated the LFG text channel for guild [{}] - [{}]", guild.getName(),
                                guild.getId());
                        BotUtils.replyToMessage(message, DONE_MESSAGE);
                        if (BotUtils.isGuildReady(guild, gc)) {
                            gc.setIsGuildReady(true);
                            guildConfigRepository.save(gc);
                            matchmakingQueuesHandler.initQueuesForGuild(guild);
                            lfgTextChannel = BotUtils.getLfgTextChannel(guild, gc, true);
                            BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, gc, lfgTextChannel,
                                    matchmakingQueuesHandler);
                        }
                    }

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setWrongTextChannel".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setWrongTextChannel(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the WrongTextChannel for guild [{}] - [{}]", guild.getName(),
                            guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase()
                        .contains(gc.getCmdPrefix() + "setUserNotInLfgVoiceChannel".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setUserNotInLfgVoiceChannel(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the UserNotInLfgVoiceChannel for guild [{}] - [{}]",
                            guild.getName(), guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setSearchingForTeammate".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setSearchingForTeammate(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the SearchingForTeammate for guild [{}] - [{}]", guild.getName(),
                            guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setWrongSyntax".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setWrongSyntax(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the WrongSyntax for guild [{}] - [{}]", guild.getName(),
                            guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setMatchmakingRoomsSyntax".toLowerCase())) {
                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setMatchMakingRoomSyntax(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the MatchmakingRoomsSyntax for guild [{}] - [{}]",
                            guild.getName(), guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "setCmdPrefix".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the config for this server! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    tmp.remove(0);
                    String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                    gc.setCmdPrefix(result);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully updated the CmdPrefix for guild [{}] - [{}]", guild.getName(),
                            guild.getId());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "addManager".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to add Managers! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    List<Manager> managers = message.getMentionedUsers().stream()
                            .map((User user) -> new Manager(user.getId(), guild.getId())).collect(Collectors.toList());
                    gc.getManagers().addAll(managers);
                    guildConfigRepository.save(gc);
                    LOGGER.info("Successfully added Managers for guild [{}] - [{}]: [{}]", guild.getName(),
                            guild.getId(), managers.toString());
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "addQueue".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the queues! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }

                    if (tmp.size() != 4) {
                        BotUtils.replyToMessage(message,
                                "Wrong syntax!\nUsage: `!addqueue [Name] [Length] [cmd to join]` e.g.: `!addqueue Duo 2 duo`");
                        return;
                    }

                    String name = tmp.get(1);
                    Integer length = new Integer(tmp.get(2));
                    String commandToJoin = tmp.get(3);

                    MatchmakingQueue queue = new MatchmakingQueue(guild.getId(), name, length, commandToJoin);
                    gc.getMatchmakingQueues().add(queue);

                    guildConfigRepository.save(gc);
                    matchmakingQueuesHandler.addQueueForGuild(guild.getId(), queue);
                    LOGGER.info("Successfully added Queue for guild [{}] - [{}]: [{}] [{}] [{}]", guild.getName(),
                            guild.getId(), name, length, commandToJoin);

                    BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, gc, lfgTextChannel,
                            matchmakingQueuesHandler);

                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }

                if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "removeQueue".toLowerCase())) {

                    if (!gc.isUserAManagerForThisGuild(author.getUser())) {
                        BotUtils.replyToMessage(message,
                                "You are not authorized to manage the queues! Other managers can add you as manager of this bot with `!addmanager @your-username-here`");
                        return;
                    }
                    String name = tmp.get(1);

                    Optional<MatchmakingQueue> opt = gc.getMatchmakingQueues().stream()
                            .filter((MatchmakingQueue queue) -> queue.getGuildId().equals(guild.getId())
                                    && queue.getName().equals(name))
                            .findFirst();
                    MatchmakingQueue queueToRemove;
                    if (opt.isPresent()) {
                        queueToRemove = opt.get();
                    } else {
                        BotUtils.replyToMessage(message, "Couldn't find queue with name `" + name + "`");
                        return;
                    }

                    gc.getMatchmakingQueues().remove(queueToRemove);
                    guildConfigRepository.save(gc);
                    matchmakingQueuesHandler.removeQueueFromGuild(guild.getId(), queueToRemove);
                    LOGGER.info("Successfully removed Queue for guild [{}] - [{}]: [{}]", guild.getName(),
                            guild.getId(), name);

                    BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, gc, lfgTextChannel,
                            matchmakingQueuesHandler);
                    BotUtils.replyToMessage(message, DONE_MESSAGE);

                    return;
                }
            } else {
                BotUtils.replyToMessage(message, "No argument supplied.");
                return;
            }

            if (tmp.get(0).toLowerCase().contains(gc.getCmdPrefix() + "join")) {

                LOGGER.debug("{}", message);

                if (!lfgVoiceChannel.getMembers().contains(author)) {
                    message.delete().queue();
                    BotUtils.replyToMessage(message, gc.getUserNotInLfgVoiceChannel());
                    return;
                }

                if (!message.getTextChannel().equals(BotUtils.getLfgTextChannel(guild, gc, false))) {
                    message.delete().queue();
                    BotUtils.replyToMessage(message, gc.getWrongTextChannel());
                    return;
                }

                tmp.remove(0);
                String result = tmp.stream().reduce((a, b) -> a + " " + b).get();

                LOGGER.debug(result);

                gc.getMatchmakingQueues().forEach((MatchmakingQueue outerQueue) -> {
                    if (outerQueue.isMessageEligibleToJoin(result)) {
                        matchmakingQueuesHandler.getQueuesForGuild(guild).stream().filter(
                                (MatchmakingQueue innerQueue) -> innerQueue.getName().equals(outerQueue.getName()))
                                .findFirst().get().add(author);
                        LOGGER.debug("Added [{}] to queue [{}]", author, outerQueue.getName());
                    }
                });

                // check for teams
                matchmakingQueuesHandler.getQueuesForGuild(guild).forEach((MatchmakingQueue queue) -> {
                    if (queue.isFull()) {
                        LOGGER.debug("Queue [{}] is full.", queue.getName());
                        List<Member> members = queue.getFirstMembers();

                        members.forEach(member -> matchmakingQueuesHandler.getQueuesForGuild(guild)
                                .forEach(tmpQueue -> tmpQueue.removeMemberFromQueue(member)));

                        // move users
                        List<VoiceChannel> availableVoiceChannels = guild.getVoiceChannels().stream().filter(
                                vc -> vc.getName().contains(gc.getMatchMakingRoomSyntax()) && vc.getMembers().isEmpty())
                                .collect(Collectors.toList());

                        LOGGER.debug("Moving members now..");
                        members.forEach(member -> guild.moveVoiceMember(member, availableVoiceChannels.get(0))
                                .queue(v -> LOGGER.debug("Success moving [{}] to [{}]", author,
                                        availableVoiceChannels.get(0)),
                                        t -> LOGGER.debug("Couldn't move [{}] to [{}]", author,
                                                availableVoiceChannels.get(0))));
                        TeamsCount count = teamsCountRepository.findOne(1);
                        count.increase();
                        teamsCountRepository.save(count);

                        GuildEventListener.updateGameOfBot(event.getJDA(),
                                teamsCountRepository.findOne(1).getTeamsFound());
                    }
                });

                message.delete().queue();

                BotUtils.replyToMessage(message, gc.getSearchingForTeammate());

                BotUtils.sendOrUpdateQueueStatsForGuildInLfgTextChannel(guild, gc, lfgTextChannel,
                        matchmakingQueuesHandler);

                return;
            }
        } else {
            if (gc.getIsGuildReady()) {

                if (message.getTextChannel().getId().equals(lfgTextChannel.getId())) {
                    message.delete().queue(
                            // success callback
                            (Void v) -> LOGGER.info("Deleted message [{}] by [{}] from guild [{}] - [{}]",
                                    message.getContentStripped(), message.getAuthor(), guild.getName(), guild.getId()),
                            // failure callback
                            (Throwable t) -> LOGGER.error("Couldn't delete message [{}] by [{}] from guild [{}] - [{}]",
                                    message.getContentStripped(), message.getAuthor(), guild.getName(), guild.getId()));
                }
            }
        }

    }

}
