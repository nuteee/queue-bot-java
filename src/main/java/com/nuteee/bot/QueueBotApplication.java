package com.nuteee.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(QueueBotApplication.class, args);
	}
}
