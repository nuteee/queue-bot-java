package com.nuteee.bot.entities;

public class MultipleOrZeroChannelsFoundExceiption extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MultipleOrZeroChannelsFoundExceiption(String message) {
		super(message);
	}

	public MultipleOrZeroChannelsFoundExceiption(String message, Throwable t) {
		super(message, t);
	}
}
