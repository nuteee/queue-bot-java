package com.nuteee.bot.entities;

import com.google.common.collect.Lists;
import com.nuteee.bot.db.entities.MatchmakingQueue;
import com.nuteee.bot.db.repositories.MatchmakingQueueRepository;
import net.dv8tion.jda.api.entities.Guild;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class MatchmakingQueuesHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchmakingQueuesHandler.class);

    @Autowired
    private MatchmakingQueueRepository matchMakingQueueRepository;

    private final Map<String, List<MatchmakingQueue>> guildToQueuesMap = new HashMap<>();
    private final Map<String, String> guildToMessageIdMap = new HashMap<>();

    public void initQueuesForGuild(Guild guild) {
        List<MatchmakingQueue> queues = Lists.newArrayList(matchMakingQueueRepository.findAll()).stream()
                .filter(queue -> guild.getId().equals(queue.getGuildId())).collect(Collectors.toList());

        guildToQueuesMap.put(guild.getId(), queues);
    }

    public List<MatchmakingQueue> getQueuesForGuild(Guild guild) {
        return guildToQueuesMap.get(guild.getId());
    }

    public String getQueueStatsForGuild(Guild guild) {
        LOGGER.debug("Getting queue stats for guild [{}]", guild.getId());
        StringBuilder sb = new StringBuilder();
        sb.append("=======================\n\n");
        guildToQueuesMap.get(guild.getId()).forEach((MatchmakingQueue queue) -> sb.append(queue).append("\n\n"));
        sb.append("=======================");
        return sb.toString();
    }

    public Boolean addQueueForGuild(String id, MatchmakingQueue queue) {

        if (!guildToQueuesMap.containsKey(id)) {
            guildToQueuesMap.put(id, new ArrayList<>());
        }

        return guildToQueuesMap.get(id).add(queue);
    }

    public Boolean removeQueueFromGuild(String id, MatchmakingQueue queue) {
        return guildToQueuesMap.get(id).remove(queue);
    }

    public void addMessageIdForGuild(String guildId, String messageId) {
        guildToMessageIdMap.put(guildId, messageId);
    }

    public void removeMessageIdForGuild(String guildid) {
        guildToMessageIdMap.remove(guildid);
    }

    public String getMessageIdForGuild(String guildId) {
        return guildToMessageIdMap.get(guildId);
    }

}
