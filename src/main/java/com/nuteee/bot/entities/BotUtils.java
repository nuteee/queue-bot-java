package com.nuteee.bot.entities;

import com.nuteee.bot.db.entities.GuildConfig;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class BotUtils {

    private BotUtils() {
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(BotUtils.class);

    public static TextChannel getLfgTextChannel(Guild guild, GuildConfig guildConfig, Boolean byPassCheck) {
        if (!guildConfig.getIsGuildReady() && !byPassCheck)
            return null;

        List<TextChannel> textChannelsByName = guild.getTextChannelsByName(guildConfig.getLfgTextChannel(), true);

        if (textChannelsByName.size() == 1) {
            return textChannelsByName.get(0);
        } else {
            LOGGER.warn("{} textchannels found for textchannel: [{}] - [{}] - [{}]",
                    textChannelsByName.isEmpty() ? "Zero" : "Multiple", guildConfig.getLfgTextChannel(),
                    guild.getName(), guild.getId());
            return null;
            // throw new MultipleOrZeroChannelsFoundExceiption(
            // String.format("%s textchannels found for textchannel: [%s] - [%s] - [%s]",
            // textChannelsByName.isEmpty() ? "Zero" : "Multiple",
            // guildConfig.getLfgTextChannel(),
            // guild.getName(), guild.getId()));
        }

    }

    public static VoiceChannel getLfgVoiceChannel(Guild guild, GuildConfig guildConfig, Boolean byPassCheck) {
        if (!guildConfig.getIsGuildReady() && !byPassCheck)
            return null;

        List<VoiceChannel> voiceChannels = guild.getVoiceChannelsByName(guildConfig.getLfgVoiceChannel(), true);

        if (voiceChannels.size() == 1) {
            return voiceChannels.get(0);
        } else {
            LOGGER.warn("{} VoiceChannels found for VoiceChannel: [{}] - [{}] - [{}]",
                    voiceChannels.isEmpty() ? "Zero" : "Multiple", guildConfig.getLfgVoiceChannel(), guild.getName(),
                    guild.getId());
            return null;
            // throw new MultipleOrZeroChannelsFoundExceiption(
            // String.format("%s VoiceChannels found for VoiceChannel: [%s] - [%s] - [%s]",
            // voiceChannels.isEmpty() ? "Zero" : "Multiple",
            // guildConfig.getLfgVoiceChannel(),
            // guild.getName(), guild.getId()));
        }
    }

    public static void sendOrUpdateQueueStatsForGuildInLfgTextChannel(Guild guild, GuildConfig guildConfig,
                                                                      TextChannel lfgTextChannel,
																	  MatchmakingQueuesHandler matchmakingQueuesHandler) {

        if (!guildConfig.getIsGuildReady()) {
            LOGGER.info("Guild [{}] is not ready yet.", guild);
            return;
        }

        String messageId = matchmakingQueuesHandler.getMessageIdForGuild(guild.getId());

		if (messageId == null) {
            lfgTextChannel.sendMessage(matchmakingQueuesHandler.getQueueStatsForGuild(guild)).queue(
                    (Message message) -> matchmakingQueuesHandler.addMessageIdForGuild(guild.getId(), message.getId()));
        } else {
            lfgTextChannel.retrieveMessageById(messageId).queue((Message message) -> message
                    .editMessage(matchmakingQueuesHandler.getQueueStatsForGuild(guild)).queue());
        }

    }

    public static void replyToMessage(Message message, String content) {
        StringBuilder sb = new StringBuilder(message.getAuthor().getAsMention()).append(" ").append(content);

        message.getTextChannel().sendMessage(sb.toString()).queue(msg -> msg.delete().queueAfter(15, TimeUnit.SECONDS));
    }

    public static Boolean isGuildReady(Guild guild, GuildConfig guildConfig) {

        if (getLfgTextChannel(guild, guildConfig, true) != null
                && getLfgVoiceChannel(guild, guildConfig, true) != null) {
            return true;
        } else {
            return false;
        }

    }

}
