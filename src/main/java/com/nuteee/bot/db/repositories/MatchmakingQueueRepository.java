package com.nuteee.bot.db.repositories;

import org.springframework.data.repository.CrudRepository;

import com.nuteee.bot.db.entities.MatchmakingQueue;

public interface MatchmakingQueueRepository extends CrudRepository<MatchmakingQueue, String> {

}
