package com.nuteee.bot.db.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TeamsCount {

	@Id
	private Integer id = 1;
	private Integer teamsFound;

	public TeamsCount() {
	}

	public TeamsCount(Integer teamsFound) {
		this.teamsFound = teamsFound;
	}

	public Integer getTeamsFound() {
		return teamsFound;
	}

	public void increase() {
		this.teamsFound++;
	}

	@Override
	public String toString() {
		return "Found " + teamsFound + " teams!";
	}

}
