package com.nuteee.bot.db.entities;

import net.dv8tion.jda.api.entities.Member;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;


@Entity
public class MatchmakingQueue {

    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchmakingQueue.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "guild_id")
    private String guildId;
    private String name;
    private Integer length;
    private String commandToJoin;

    @Transient
    private final Deque<Member> queue;

    public MatchmakingQueue() {
        this.queue = new ArrayDeque<>();
    }

    public MatchmakingQueue(String guildId, String name, Integer length, String commandToJoin) {
        this.guildId = guildId;
        this.name = name;
        this.length = length;
        this.commandToJoin = commandToJoin;
        this.queue = new ArrayDeque<>();
    }

    public static List<MatchmakingQueue> getDefaultMatchmakingQueuesForGuildId(String guildId) {
        return Arrays.asList(new MatchmakingQueue(guildId, "Duo", 2, "duo"),
                new MatchmakingQueue(guildId, "Squad", 4, "squad"));
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getLength() {
        return length;
    }

    public String getCommandToJoin() {
        return commandToJoin;
    }

    public String getGuildId() {
        return guildId;
    }

    public Deque<Member> getQueue() {
        return queue;
    }

    public List<Member> getFirstMembers() {
        List<Member> users = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            users.add(queue.pop());
        }

        return users;
    }

    public MatchmakingQueue add(Member member) {
        if (!queue.contains(member)) {
            queue.addLast(member);
        }
        return this;
    }

    public Boolean isMessageEligibleToJoin(String message) {
        LOGGER.debug("{} IN {} == {}", this.commandToJoin, message,
                new ArrayList<String>(Arrays.asList(message.split("\\s+"))).stream()
                        .anyMatch((String str) -> commandToJoin.equals(str)));

        return new ArrayList<String>(Arrays.asList(message.split("\\s+"))).stream()
                .anyMatch((String str) -> commandToJoin.equals(str));
    }

    public Boolean removeMemberFromQueue(Member member) {
        return queue.removeFirstOccurrence(member);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.guildId).append(this.name).append(this.length)
                .append(this.commandToJoin).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || !(obj instanceof MatchmakingQueue))
            return false;

        MatchmakingQueue other = (MatchmakingQueue) obj;

        return new EqualsBuilder().append(this.guildId, other.guildId).append(this.name, other.name)
                .append(this.length, other.length).append(this.commandToJoin, other.commandToJoin).build();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.name + ":");
        sb.append("\t");
        sb.append(this.queue.size() + " / " + this.length);
        sb.append("\t\t");
        sb.append("`!join " + this.commandToJoin + "`");

        return sb.toString();
    }

    public boolean isFull() {
        return this.queue.size() % this.length == 0 && !this.queue.isEmpty();
    }

}
