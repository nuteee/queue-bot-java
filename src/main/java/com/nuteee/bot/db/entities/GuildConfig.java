package com.nuteee.bot.db.entities;

import net.dv8tion.jda.api.entities.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;


@Entity
public class GuildConfig {

    @Id
    @Column(name = "guild_id")
    private String guildId;

    private String lfgVoiceChannel;
    private String lfgTextChannel;
    private String wrongTextChannel;
    private String userNotInLfgVoiceChannel;
    private String searchingForTeammate;
    private String wrongSyntax;
    private String matchMakingRoomSyntax;
    private String cmdPrefix;
    private Boolean isGuildReady;

    private Timestamp validFrom = new Timestamp(System.currentTimeMillis());

    private Timestamp validTo = Timestamp.valueOf("9999-12-31 01:01:01");

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "guild_id", referencedColumnName = "guild_id")
    private List<Manager> managers;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "guild_id", referencedColumnName = "guild_id")
    private List<MatchmakingQueue> matchmakingQueues;

    /**
     * Needed for Hibernate..
     */
    public GuildConfig() {
    }

    public GuildConfig(String id, String lfgVoiceChannel, String lfgTextChannel, String wrongTextChannel,
                       String userNotInLfgVoiceChannel, String searchingForTeammate, String wrongSyntax,
                       String matchMakingRoomSyntax, String cmdPrefix, Boolean isGuildReady) {
        this.guildId = id;
        this.lfgVoiceChannel = lfgVoiceChannel;
        this.lfgTextChannel = lfgTextChannel;
        this.wrongTextChannel = wrongTextChannel;
        this.userNotInLfgVoiceChannel = userNotInLfgVoiceChannel;
        this.searchingForTeammate = searchingForTeammate;
        this.wrongSyntax = wrongSyntax;
        this.matchMakingRoomSyntax = matchMakingRoomSyntax;
        this.cmdPrefix = cmdPrefix;
        this.isGuildReady = isGuildReady;
    }

    public static GuildConfig getDefaultGuildConfigForGuildId(String guildId) {
        return new GuildConfig(guildId, "LFG", "matchmaking",
                "You can only use this command in the #bot-commands channel",
                "You need to connect to the lfg-squad voice channel first.", "Searching for your teammate(s)!",
                "Wrong syntax! You can use these parameters: [fpp, duo, squad]", "|| matchmaking", "!", false);
    }

    public static GuildConfig getDefaultGuildConfigForGuildIdAndManagers(String guildId, List<String> managerIds) {
        GuildConfig gc = getDefaultGuildConfigForGuildId(guildId);
        List<Manager> managers = managerIds.stream().map((String managerId) -> new Manager(managerId, guildId))
                .collect(Collectors.toList());
        gc.setManagers(managers);
        gc.setMatchmakingQueues(MatchmakingQueue.getDefaultMatchmakingQueuesForGuildId(guildId));
        return gc;
    }

    public String getId() {
        return this.guildId;
    }

    public String getLfgVoiceChannel() {
        return lfgVoiceChannel;
    }

    public void setLfgVoiceChannel(String lfgVoiceChannel) {
        this.lfgVoiceChannel = lfgVoiceChannel;
    }

    public String getLfgTextChannel() {
        return lfgTextChannel;
    }

    public void setLfgTextChannel(String lfgTextChannel) {
        this.lfgTextChannel = lfgTextChannel;
    }

    public String getWrongTextChannel() {
        return wrongTextChannel;
    }

    public void setWrongTextChannel(String wrongTextChannel) {
        this.wrongTextChannel = wrongTextChannel;
    }

    public String getUserNotInLfgVoiceChannel() {
        return userNotInLfgVoiceChannel;
    }

    public void setUserNotInLfgVoiceChannel(String userNotInLfgVoiceChannel) {
        this.userNotInLfgVoiceChannel = userNotInLfgVoiceChannel;
    }

    public String getSearchingForTeammate() {
        return searchingForTeammate;
    }

    public void setSearchingForTeammate(String searchingForTeammate) {
        this.searchingForTeammate = searchingForTeammate;
    }

    public String getWrongSyntax() {
        return wrongSyntax;
    }

    public void setWrongSyntax(String wrongSyntax) {
        this.wrongSyntax = wrongSyntax;
    }

    public String getMatchMakingRoomSyntax() {
        return matchMakingRoomSyntax;
    }

    public void setMatchMakingRoomSyntax(String matchMakingRoomSyntax) {
        this.matchMakingRoomSyntax = matchMakingRoomSyntax;
    }

    public String getCmdPrefix() {
        return cmdPrefix;
    }

    public void setCmdPrefix(String cmdPrefix) {
        this.cmdPrefix = cmdPrefix;
    }

    public List<Manager> getManagers() {
        return this.managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public Timestamp getValidFrom() {
        return this.validFrom;
    }

    public void setValidFrom(Timestamp ts) {
        this.validFrom = ts;
    }

    public Timestamp getValidTo() {
        return this.validTo;
    }

    public void setValidTo(Timestamp ts) {
        this.validTo = ts;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public List<MatchmakingQueue> getMatchmakingQueues() {
        return matchmakingQueues;
    }

    public void setMatchmakingQueues(List<MatchmakingQueue> matchmakingQueues) {
        this.matchmakingQueues = matchmakingQueues;
    }

    public Boolean getIsGuildReady() {
        return isGuildReady;
    }

    public void setIsGuildReady(Boolean isGuildReady) {
        this.isGuildReady = isGuildReady;
    }

    public String getPropertyValues() {
        StringBuilder sb = new StringBuilder();

        sb.append("lfgVoiceChannelName: `" + this.lfgVoiceChannel + "`\n");
        sb.append("lfgTextChannelName: `" + this.lfgTextChannel + "`\n");
        sb.append("wrongTextChannel: `" + this.wrongTextChannel + "`\n");
        sb.append("userNotInLfgVoiceChannel: `" + this.userNotInLfgVoiceChannel + "`\n");
        sb.append("searchingForTeammate: `" + this.searchingForTeammate + "`\n");
        sb.append("wrongSyntax: `" + this.wrongSyntax + "`\n");
        sb.append("matchmakingRoomsSyntax: `" + this.matchMakingRoomSyntax + "`\n");
        sb.append("cmdPrefix: `" + this.cmdPrefix + "`\n");

        sb.append("managers: " + this.managers.stream().map((Manager manager) -> "<@" + manager.getUserId() + "> ")
                .collect(Collectors.toList()).toString());

        return sb.toString();
    }

    public Boolean isUserAManagerForThisGuild(User user) {
        List<String> managerIds = this.getManagers().stream().map((Manager manager) -> manager.getUserId())
                .collect(Collectors.toList());
        return managerIds.contains(user.getId());
    }

    @Override
    public String toString() {
        return "Guild: [ " + this.guildId + "]";
    }

}
